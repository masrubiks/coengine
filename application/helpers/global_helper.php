<?php
if (!function_exists('format_actionbutton')) {
    function format_actionbutton($data)
    {
        $action_button = '<div class="btn-group">';
        foreach ($data as $key => $val) {
            switch ($val['button']) {
                case 'edit':
                    $action_button .= '<a href="javascript:void(0)" class="btn btn-sm btn-flat btn-success edit-data" data-toggle="modal" data-target="#modal-edit" data-id="' . $val['id'] . '" title="Edit"><i class="fa fa-fw fa-edit"></i></a>';
                    break;
                case 'delete':
                    $action_button .= '<a href="javascript:void(0)" class="btn btn-sm btn-flat btn-danger btn-delete delete-data" title="Delete" data-id="' . $val['id'] . '" data-name="' . $val['name'] . '"><i class="fa fa-fw fa-times"></i></a>';
                    break;
                case 'detail':
                    $action_button .= '<a href="javascript:void(0)" class="btn btn-sm btn-flat btn-info" title="Detail"><i class="fa fa-fw fa-info-circle"></i></a>';
                    break;
            }
        };
        $action_button .= '</div>';
        return $action_button;
    }
}
