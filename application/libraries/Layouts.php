<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Layouts
{
    var $CI;

    function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->load->library('session');
    }

    function is_ajax()
    {
        if (!$this->CI->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
    }

    function templates($view = 'templates/v_blank', $data = null, $head = null, $foot = null)
    {
        $config = $this->CI->m_general->get_row_object('engine_config');
        $header['config'] = $config;
        $sidebar['config'] = $config;
        $footer['config'] = $config;
        $urls = 'templates/' . $config->config_template . '/';

        $sidebar['active_menu'] = $this->CI->router->fetch_class();
        //$role = $this->CI->session->userdata('USER_ROLE_ID');
        $role = 1;
        $list_menu = $this->CI->m_general->getMenuParent($role);

        foreach ($list_menu as $key => $value) {
            $child = $this->CI->m_general->getMenuChild($role, $value->menu_id);
            if ($child) {
                $list_menu[$key]->child = $child; //
                foreach ($child as $key2 => $value2) {
                    $grandChild = $this->CI->m_general->getMenuChild($role, $value2->menu_id);
                    if ($grandChild) {
                        $list_menu[$key]->child[$key2]->grandChild = $grandChild;
                        foreach ($grandChild as $key3 => $value3) {
                            if ($value3->module_name == $sidebar['active_menu']) {
                                $value3->active = true;
                                $value2->expand = true;
                                $value->expand = true;
                            }
                        }
                    } else {
                        if ($value2->module_name == $sidebar['active_menu']) {
                            $value2->active = true;
                            $value->expand = true;
                        }
                    }
                }
            } else {
                if ($value->module_name == $sidebar['active_menu']) {
                    $value->active = true;
                }
            }
        }
        $sidebar['list_menu'] = $list_menu;

        $header['head'] = $head;
        $header['config'] = $config;
        $footer['foot'] = $foot;
        $footer['config'] = $config;

        $this->CI->load->view($urls . 'v_header', $header);
        $this->CI->load->view($urls . 'v_sidebar', $sidebar);
        $this->CI->load->view($view, $data);
        $this->CI->load->view($urls . 'v_footer', $footer);
    }

    function blank_templates($data = null)
    {
        $this->CI->load->view('templates/v_blank', $data);
    }

    function make_breadcrumb($datas = null)
    {
        $home = array(
            'name' => 'Home',
            'url' => 'home'
        );

        if ($datas) {
            $data = array($home, $datas);
        } else {
            $data = array($home);
        }

        $config = $this->CI->m_general->get_row_object('engine_config');

        if ($config->config_template == 'adminlte') {
            $str = '<ol class="breadcrumb float-sm-right">';
            $lastElement = end($data);

            foreach ($data as $key => $val) {
                if ($val !== $lastElement) {
                    $str .= '<li class="breadcrumb-item">';
                    $str .= '<a href="' . $val['url'] . '"><i class="fas fa-home"></i> ' . $val['name'] . '</a></li>';
                } else {
                    $str .= '<li class="breadcrumb-item">';
                    if ($val['name'] == 'Home') {
                        $str .= '<i class="fas fa-home"></i> ' . $val['name'] . '</li>';
                    } else {
                        $str .= $val['name'] . '</li>';
                    }
                }
            }
            $str .= '</ol>';
            return $str;
        } else if ($config->config_template == 'adminator') {
        } else {
        }
    }
}
