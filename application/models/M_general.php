<?php

class M_general extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_object($table)
    {
        if (empty($table))
            return false;
        return $this->db->get($table)->result();
    }

    public function get_array($table)
    {
        if (empty($table))
            return false;
        return $this->db->get($table)->result_array();
    }

    public function get_where_object($table, $where)
    {
        if (empty($table) || empty($where))
            return false;
        foreach ($where as $key => $value) {
            $this->db->where($key, $value);
        }
        return $this->db->get($table)->result();
    }

    public function get_where_array($table, $where)
    {
        if (empty($table) || empty($where))
            return false;
        foreach ($where as $key => $value) {
            $this->db->where($key, $value);
        }
        return $this->db->get($table)->result_array();
    }

    public function get_where_order_object($table, $where, $order)
    {
        if (empty($table) || empty($where))
            return false;
        foreach ($where as $key => $value) {
            $this->db->where($key, $value);
        }

        foreach ($order as $key2 => $value2) {
            $this->db->order_by($key2, $value2);
        }

        return $this->db->get($table)->result();
    }

    public function get_where_order_array($table, $where, $order)
    {
        if (empty($table) || empty($where))
            return false;
        foreach ($where as $key => $value) {
            $this->db->where($key, $value);
        }

        foreach ($order as $key2 => $value2) {
            $this->db->order_by($key2, $value2);
        }
        
        return $this->db->get($table)->result_array();
    }


    public function get_row_object($table)
    {
        if (empty($table))
            return false;
        return $this->db->get($table)->row();
    }

    public function get_row_array($table)
    {
        if (empty($table))
            return false;
        return $this->db->get($table)->row_array();
    }

    public function get_where_row_object($table, $where)
    {
        if (empty($table) || empty($where))
            return false;
        foreach ($where as $key => $value) {
            $this->db->where($key, $value);
        }
        return $this->db->get($table)->row();
    }

    public function get_where_row_array($table, $where)
    {
        if (empty($table) || empty($where))
            return false;
        foreach ($where as $key => $value) {
            $this->db->where($key, $value);
        }
        return $this->db->get($table)->row_array();
    }

    public function insert_data($tabel, $data)
    {
        if (empty($tabel) || empty($data))
            return false;
        return $this->db->insert($tabel, $data);
    }

    public function insert_data_last_id($tabel, $data)
    {
        if (empty($tabel) || empty($data))
            return false;
        $this->db->insert($tabel, $data);
        return $this->db->insert_id();
    }

    public function insert_data_batch($tabel, $data)
    {
        if (empty($tabel) || empty($data))
            return false;
        return $this->db->insert_batch($tabel, $data);
    }

    public function last_id()
    {
        return $this->db->insert_id();
    }

    public function update_data($table, $data, $where)
    {
        if (empty($table) || empty($data) || empty($where))
            return false;
        foreach ($where as $key => $value) {
            $this->db->where($key, $value);
        }
        return $this->db->update($table, $data);
    }

    public function update_data_batch($table, $data, $column)
    {
        if (empty($table) || empty($data) || empty($column))
            return false;
        return $this->db->update_batch($table, $data, $column);
    }

    public function delete_data($table, $data)
    {
        if (empty($table) || empty($data))
            return false;
        return $this->db->delete($table, $data);
    }

    public function delete_data_batch($table, $selection, $data)
    {
        if (empty($table) || empty($data))
            return false;
        $this->db->where_in($selection, $data);
        return $this->db->delete($table);
    }

    public function count_data($table)
    {
        if (empty($table))
            return false;
        return $this->db->get($table)->num_rows();
    }

    public function count_data_where($table, $data)
    {
        if (empty($table) || empty($data))
            return false;
        return $this->db->get_where($table, $data)->num_rows();
    }

    public function dropdown_data($table, $id, $name)
    {
        if (empty($table) || empty($id) || empty($name))
            return false;
        $this->db->select("$id as id");
        $this->db->select("$name as text");
        $this->db->from($table);
        return $this->db->get()->result();
    }

    public function dropdown_data_where($table, $id, $name, $where)
    {
        if (empty($table) || empty($id) || empty($name) || empty($where))
            return false;
        $this->db->select("$id as id");
        $this->db->select("$name as text");
        $this->db->from($table);
        foreach ($where as $key => $value) {
            $this->db->where($key, $value);
        }
        return $this->db->get()->result();
    }

    // data table

    public function get_table($data)
    {
        $this->get_table_query($data);

        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);

        $query = $this->db->get();
        $list = $query->result();

        $values = array();
        $no = $_POST['start'];
        foreach ($list as $rows) {
            $no++;
            $row = array();
            $row[] = $no;

            foreach ($data['column_show'] as $val) {
                if (is_array($val)) {
                    foreach ($val as $value => $format) {
                        $row[] = $format($rows->$value);
                    }
                } else {
                    $row[] = $rows->$val;
                }
            }

            $values[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->db->count_all_results($data['table']),
            "recordsFiltered" => $this->get_table_count($data),
            "data" => $values,
        );
        return $output;
    }

    public function get_table_query($data)
    {
        if (isset($data['select'])) {
            $this->db->select($data['select']);
        }

        $this->db->from($data['table']);

        if (isset($data['join'])) {
            foreach ($data['join'] as $key => $val) {
                $this->db->join($key, $val[0], $val[1]);
            }
        }

        if (isset($data['where'])) {
            foreach ($data['where'] as $key => $val) {
                $this->db->where($key, $val);
            }
        }

        if (isset($data['group_by'])) {
            foreach ($data['group_by'] as $val) {
                $this->db->group_by($val);
            }
        }

        $i = 0;
        foreach ($data['column_search'] as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($data['column_search']) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($_POST['order']['0']['column'], $_POST['order']['0']['dir']);
        } else if (isset($data['orders'])) {
            $order = $data['orders'];
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_table_count($data)
    {
        $this->get_table_query($data);
        return $this->db->get()->num_rows();
    }

    // menu otomatis
    public function getmenuparent($role_id)
    {
        $this->db->select('
        b.menu_id,
        b.menu_name,
        b.menu_icon,
        b.menu_parent_id,
        b.menu_module_id,
        c.module_name
    ');
        $this->db->from('engine_menuaccess a');
        $this->db->join('engine_menu b', 'b.menu_id = a.menuaccess_menu_id', 'left');
        $this->db->join('engine_module c', 'c.module_id = b.menu_module_id', 'left');
        $this->db->where('a.menuaccess_role_id', $role_id);
        $this->db->where('b.menu_active', '1');
        $this->db->where('b.menu_parent_id', null);
        $this->db->order_by('b.menu_order', 'asc');
        $this->db->order_by('b.menu_id', 'asc');
        $query = $this->db->get();
        return $query->result();
    }

    public function getmenuchild($role_id, $menu_parent_id)
    {
        $this->db->select('
        b.menu_id,
        b.menu_name,
        b.menu_icon,
        b.menu_parent_id,
        b.menu_module_id,
        c.module_name
    ');
        $this->db->from('engine_menuaccess a');
        $this->db->join('engine_menu b', 'b.menu_id = a.menuaccess_menu_id', 'left');
        $this->db->join('engine_module c', 'c.module_id = b.menu_module_id', 'left');
        $this->db->where('a.menuaccess_role_id', $role_id);
        $this->db->where('b.menu_parent_id', $menu_parent_id);
        $this->db->where('b.menu_active', '1');
        $this->db->order_by('b.menu_order', 'asc');
        $this->db->order_by('b.menu_id', 'asc');
        $query = $this->db->get();
        return $query->result();
    }

    public function getmoduleaccess($role_id, $module_name)
    {
        $this->db->select('
        c.`module_name`
    ');
        $this->db->from('engine_menuaccess a');
        $this->db->join('engine_menu b', 'b.`menu_id` = a.`menuaccess_menu_id`');
        $this->db->join('engine_module c', 'c.`module_id` = b.`menu_module_id`');
        $this->db->where('a.menuaccess_role_id', $role_id);
        $this->db->where('c.module_name', $module_name);
        $get_access = $this->db->get();
        return $get_access->result_array();
    }

    public function get_menu($param = 'home')
    {
        $this->db->select('b.menu_name, b.menu_icon,');
        $this->db->from('engine_module a');
        $this->db->join('engine_menu b', 'b.menu_module_id = a.module_id', 'left');
        $this->db->where('a.module_name', $param);
        $query = $this->db->get();
        return $query->row();
    }
}
