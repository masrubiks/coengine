<?php

class Home extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $menu = $this->m_general->get_menu();

        $data['title'] = $menu->menu_name;
        $data['icon'] = $menu->menu_icon;

        $data['breadcrumb'] = $this->layouts->make_breadcrumb();
        $this->layouts->templates('v_index', $data);
    }

    public function create_log()
    {
        $this->layouts->is_ajax();
        $this->db->trans_begin();

        $data_insert['log_timestamp'] = date('Y-m-d H:i:s');
        $data_insert['log_urls'] = $this->input->post('urls');
        $data_insert['log_user'] = 'me';

        $this->m_general->insert_data('engine_log', $data_insert);

        if ($this->db->trans_status()) {
            $this->db->trans_commit();
        } else {
            $this->db->trans_rollback();
        }
    }
}
