<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Crud extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    function get($tabel)
    {
        if (empty($tabel))
            return false;
        if ($this->db->get($tabel)->num_rows() > 0) {
            return $this->db->get($tabel)->result_array();
        } else {
            return $this->db->get($tabel)->row_array();
        }
    }

    function get_where($tabel, $data)
    {
        if (empty($tabel) || empty($data))
            return false;
        if ($this->db->get_where($tabel, $data)->num_rows() > 0) {
            return $this->db->get_where($tabel, $data)->result_array();
        } else {
            return $this->db->get_where($tabel, $data)->row_array();
        }
    }

    function get_where_result($tabel, $data)
    {
        if (empty($tabel) || empty($data))
            return false;
        return $this->db->get_where($tabel, $data)->result();
    }

    function get_data_where_order($table, $where, $order_column, $order_type)
    {
        foreach ($where as $key => $value) {
            $this->db->where($key, $value);
        }
        $this->db->order_by("$order_column", "$order_type");
        $query = $this->db->get("$table");
        $result = $query->result();
        $this->db->save_queries = false;

        return $result;
    }

    function get_row($tabel)
    {
        if (empty($tabel))
            return false;
        return $this->db->get($tabel)->row();
    }

    function get_where_row($tabel, $data)
    {
        if (empty($tabel) || empty($data))
            return false;
        return $this->db->get_where($tabel, $data)->row();
    }

    function insert($tabel, $data)
    {
        if (empty($tabel) || empty($data))
            return false;
        return $this->db->insert($tabel, $data);
    }

    function insert_batch($tabel, $data)
    {
        if (empty($tabel) || empty($data))
            return false;
        return $this->db->insert_batch($tabel, $data);
    }

    function update($tabel, $kolom, $id, $data)
    {
        if (empty($tabel) || empty($data) || empty($kolom) || empty($id))
            return false;
        $this->db->where($kolom, $id);
        return $this->db->update($tabel, $data);
    }

    function update_batch($tabel, $data, $kolom)
    {
        if (empty($tabel) || empty($data))
            return false;
        return $this->db->update_batch($tabel, $data, $kolom);
    }

    function update_any_where($table, $data, $where)
    {
        foreach ($where as $key => $value) {
            $this->db->where($key, $value);
        }
        $this->db->update("$table", $data);

        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }

    function delete($tabel, $parameter)
    {
        if (empty($tabel) or empty($parameter))
            return false;
        return $this->db->delete($tabel, $parameter);
    }

    function delete_batch($tabel, $selection, $parameter)
    {
        if (empty($tabel) or empty($parameter))
            return false;
        $this->db->where_in($selection, $parameter);
        return $this->db->delete($tabel);
    }

    function last_id()
    {
        return $this->db->insert_id();
    }

    function count($tabel)
    {
        if (empty($tabel))
            return false;
        return $this->db->get($tabel)->num_rows();
    }

    function count_where($tabel, $data)
    {
        if (empty($tabel) || empty($data))
            return false;
        return $this->db->get_where($tabel, $data)->num_rows();
    }

    // data table

    public function get_table($data)
    {
        $this->get_table_query($data);

        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);

        $query = $this->db->get();
        $list = $query->result();

        $values = array();
        $no = $_POST['start'];
        foreach ($list as $rows) {
            $no++;
            $row = array();
            $row[] = $no;

            foreach ($data['column_show'] as $val) {
                if (is_array($val)) {
                    foreach ($val as $value => $format) {
                        $row[] = $format($rows->$value);
                    }
                } else {
                    $row[] = $rows->$val;
                }
            }

            $values[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->db->count_all_results($data['table']),
            "recordsFiltered" => $this->get_table_count($data),
            "data" => $values,
        );
        return $output;
    }

    public function get_table_query($data)
    {
        if (isset($data['select'])) {
            $this->db->select($data['select']);
        }

        $this->db->from($data['table']);

        if (isset($data['join'])) {
            foreach ($data['join'] as $key => $val) {
                $this->db->join($key, $val[0], $val[1]);
            }
        }

        if (isset($data['where'])) {
            foreach ($data['where'] as $key => $val) {
                $this->db->where($key, $val);
            }
        }

        if (isset($data['group_by'])) {
            foreach ($data['group_by'] as $val) {
                $this->db->group_by($val);
            }
        }

        $i = 0;
        foreach ($data['column_search'] as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($data['column_search']) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($_POST['order']['0']['column'], $_POST['order']['0']['dir']);
        } else if (isset($data['orders'])) {
            $order = $data['orders'];
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_table_count($data)
    {
        $this->get_table_query($data);
        return $this->db->get()->num_rows();
    }

    // menu otomatis
    public function getmenuparent($role_id)
    {
        $this->db->select('
			b.menu_id,
			b.menu_name,
			b.menu_icon,
			b.menu_parent_id,
			b.menu_module_id,
			c.module_name
		');
        $this->db->from('engine_menuaccess a');
        $this->db->join('engine_menu b', 'b.menu_id = a.menuaccess_menu_id', 'left');
        $this->db->join('engine_module c', 'c.module_id = b.menu_module_id', 'left');
        $this->db->where('a.menuaccess_role_id', $role_id);
        $this->db->where('b.menu_active', '1');
        $this->db->where('b.menu_parent_id', null);
        $this->db->order_by('b.menu_order', 'asc');
        $this->db->order_by('b.menu_id', 'asc');
        $query = $this->db->get();
        return $query->result();
    }

    public function getmenuchild($role_id, $menu_parent_id)
    {
        $this->db->select('
			b.menu_id,
			b.menu_name,
			b.menu_icon,
			b.menu_parent_id,
			b.menu_module_id,
			c.module_name
		');
        $this->db->from('engine_menuaccess a');
        $this->db->join('engine_menu b', 'b.menu_id = a.menuaccess_menu_id', 'left');
        $this->db->join('engine_module c', 'c.module_id = b.menu_module_id', 'left');
        $this->db->where('a.menuaccess_role_id', $role_id);
        $this->db->where('b.menu_parent_id', $menu_parent_id);
        $this->db->where('b.menu_active', '1');
        $this->db->order_by('b.menu_order', 'asc');
        $this->db->order_by('b.menu_id', 'asc');
        $query = $this->db->get();
        return $query->result();
    }

    public function getmoduleaccess($role_id, $module_name)
    {
        $this->db->select('
            c.`module_name`
        ');
        $this->db->from('engine_menuaccess a');
        $this->db->join('engine_menu b', 'b.`menu_id` = a.`menuaccess_menu_id`');
        $this->db->join('engine_module c', 'c.`module_id` = b.`menu_module_id`');
        $this->db->where('a.menuaccess_role_id', $role_id);
        $this->db->where('c.module_name', $module_name);
        $get_access = $this->db->get();
        return $get_access->result_array();
    }

    public function get_menu($param)
    {
        $this->db->select('b.menu_name, b.menu_icon,');
        $this->db->from('engine_module a');
        $this->db->join('engine_menu b', 'b.menu_module_id = a.module_id', 'left');
        $this->db->where('a.module_name', $param);
        $query = $this->db->get();
        return $query->row();
    }
}
