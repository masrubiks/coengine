<?php

class Sys_config extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $menu = $this->m_general->get_menu($this->uri->segment(1));

        $data['title'] = $menu->menu_name;
        $data['icon'] = $menu->menu_icon;

        $breadcrumb = array(
            'name' => $menu->menu_name,
            'url' => $this->uri->segment(1),
        );

        $data['breadcrumb'] = $this->layouts->make_breadcrumb($breadcrumb);
        $data['config'] = $this->m_general->get_row_object('engine_config');
        $data['template'] = $this->m_general->get_where_object('engine_master', array('master_category' => 'template'));

        $this->layouts->templates('v_index', $data, null, 'v_foot');
    }

    public function save_edit()
    {
        $this->layouts->is_ajax();

        $this->db->trans_begin();

        $data_update['config_name'] = $this->input->post('config_name');
        $data_update['config_site'] = $this->input->post('config_site');
        $data_update['config_dev'] = $this->input->post('config_dev');
        $data_update['config_contact'] = $this->input->post('config_contact');
        $data_update['config_email'] = $this->input->post('config_email');
        $data_update['config_description'] = $this->input->post('config_description');
        $data_update['config_template'] = $this->input->post('config_template');
        $data_update['update_timestamp'] = date('Y-m-d H:i:s');
        $data_update['update_user_id'] = 'me';

        $this->m_general->update_data('engine_config', $data_update, array('config_id' => $this->input->post('config_id')));

        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            $result['status'] = true;
            $result['message'] = 'Data berhasil di ubah';
        } else {
            $this->db->trans_rollback();
            $result['status'] = false;
            $result['message'] = 'Data gagal di ubah';
        }

        $this->output->set_output(json_encode($result));
    }
}
