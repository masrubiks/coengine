<script>
    $(document).ready(function() {
        $('#form-edit').submit(function() {
            $.ajax({
                url: "<?php echo base_url('sys_config/save_edit'); ?>",
                type: "POST",
                data: $(this).serialize(),
                timeout: 5000,
                dataType: "JSON",
                success: function(data) {
                    if (data.status) {
                        toastr.success(data.message);
                    } else {
                        toastr.error(data.message);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    toastr.error('Terjadi kesalahan saat menghubungkan ke server.');
                }
            });
        });
    });
</script>