<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><i class="<?= $icon; ?> "></i> <?= $title; ?></h1>
                </div>
                <div class="col-sm-6">
                    <?= $breadcrumb; ?>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">Data <?= $title; ?></h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                </div>
            </div>
            <div class="card-body">
                <form class="form" method="post" onsubmit="return false" id="form-edit">
                    <div class="col-lg-12 col-sm-12 col-xs-12">
                        <div class="row">
                            <div class="col-lg-6 col-sm-6 col-xs-12">
                                <input type="hidden" name="config_id" class="form-control" value="<?= $config->config_id; ?>">
                                <div class="form-group">
                                    <label for="inputName">Project Name</label>
                                    <input type="text" required name="config_name" class="form-control" value="<?= $config->config_name; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="inputName">Project Site</label>
                                    <input type="text" required name="config_site" class="form-control" value="<?= $config->config_site; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="inputName">Project Developer</label>
                                    <input type="text" required name="config_dev" class="form-control" value="<?= $config->config_dev; ?>">
                                </div>
                            </div>
                            <div class="col-lg-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="inputName">Project Contact</label>
                                    <input type="number" required name="config_contact" class="form-control" value="<?= $config->config_contact; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="inputName">Project Email</label>
                                    <input type="email" required name="config_email" class="form-control" value="<?= $config->config_email; ?>">
                                </div>
                                <div class="form-group">
                                    <label>Project Template</label>
                                    <select required name="config_template" class="form-control custom-select">
                                        <option selected="" disabled="">Select one</option>
                                        <?php foreach ($template as $row) : ?>
                                            <option <?php if ($config->config_template == $row->master_value) {
                                                        echo "selected";
                                                    } ?> value="<?= $row->master_value; ?>"> <?= $row->master_label; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputDescription">Project Description</label>
                            <textarea required name="config_description" class="form-control" rows="4"><?= $config->config_description; ?></textarea>
                        </div>
                        <button class="btn btn-info btn-flat float-right" type="submit">
                            <i class="fa fa-fw fa-check"></i> Save
                        </button>
                    </div>
                </form>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->