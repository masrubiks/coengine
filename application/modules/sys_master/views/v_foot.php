<!-- DataTables -->
<script src="<?= base_url('assets/' . $config->config_template . '/'); ?>plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url('assets/' . $config->config_template . '/'); ?>plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
    $(document).ready(function() {
        dataTableVar = $('#table-datatable').DataTable({
            //dom: 'Blfrtip',
            lengthMenu: [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            autoWidth: true,
            processing: true,
            scrollX: true,
            serverSide: true,
            ordering: true,
            order: [1, 'asc'],
            ajax: {
                url: '<?= $url_table; ?>',
                type: 'POST'
            },
            columns: [{
                    width: '1%',
                    orderable: false,
                    className: 'text-center'
                },
                {
                    orderable: true,
                    visible: false
                },
                {
                    orderable: true
                },
                {
                    orderable: true
                },
                {
                    orderable: true
                },
                {
                    width: '1%',
                    orderable: false,
                    className: 'text-center'
                }
            ]
        });
    });

    $('#form-add').submit(function() {
        var modal_id = '#modal-add';
        var modal_form = '#form-add';

        $.ajax({
            url: "<?php echo base_url('sys_master/save_add'); ?>",
            type: "POST",
            data: $(this).serialize(),
            timeout: 5000,
            dataType: "JSON",
            success: function(data) {
                if (data.status) //if success close modal and reload ajax table
                {
                    dataTableVar.ajax.reload();
                    toastr.success(data.message);

                    $(modal_id).modal('hide');
                    $(modal_form)[0].reset();
                } else {
                    toastr.error(data.message);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                toastr.error('Terjadi kesalahan saat menghubungkan ke server.');
            }
        });
    });
    
    $('#table-datatable').on('click', '.edit-data', function () {
            var form = '#form-edit';
            var data_id = $(this).attr('data-id');
            $.ajax({
                url: "<?php echo base_url('sys_master/get') ?>",
                type: "POST",
                data: "data_id=" + data_id,
                timeout: 5000,
                dataType: "JSON",
                success: function (data) {
                    if (data.status) {
                        $(form + ' [name="master_id"]').val(data.data.master_id);
                        $(form + ' [name="master_label"]').val(data.data.master_label);
                        $(form + ' [name="master_value"]').val(data.data.master_value);
                        $(form + ' [name="master_category"]').val(data.data.master_category);
                    } else {
                        toastr.error(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    dataTableVar.ajax.reload();
                    toastr.error('Terjadi kesalahan saat menghubungkan ke server.');
                }
            });
        });

        $('#form-edit').submit(function () {
            var modal_id = '#modal-edit';
            var modal_form = '#form-edit';

            $.ajax({
                url: "<?php echo base_url('sys_master/save_edit'); ?>",
                type: "POST",
                data: $(this).serialize(),
                timeout: 5000,
                dataType: "JSON",
                success: function (data) {
                    if (data.status) //if success close modal and reload ajax table
                    {
                        dataTableVar.ajax.reload();
                        toastr.success(data.message);

                        $(modal_id).modal('hide');
                        $(modal_form)[0].reset();
                    } else {
                        toastr.error(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error('Terjadi kesalahan saat menghubungkan ke server.');
                }
            });
        });

        $('#table-datatable').on('click', '.delete-data', function() {
        var data_id = $(this).attr('data-id');
        var data_name = $(this).attr('data-name');

        bootbox.confirm({
            closeButton: false,
            message: "Are you sure to delete " + data_name + "?",
            callback: function(result) {
                if (result) {
                    $.ajax({
                        url: "<?php echo base_url('sys_master/delete') ?>",
                        type: "POST",
                        data: "data_id=" + data_id,
                        timeout: 5000,
                        dataType: "JSON",
                        success: function(data) {
                            if (data.status) {
                                toastr.success(data.message);
                                dataTableVar.ajax.reload();
                            } else {
                                toastr.error(data.message);
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            dataTableVar.ajax.reload();
                            toastr.error('Terjadi kesalahan saat menghubungkan ke server.');
                        }
                    });
                }
            }
        })
    });
</script>