<input class="form-control" name="master_id" type="hidden">
<div class="form-group">
    <label class="control-label">Master Label</label>
    <input autocomplete="off" class="form-control" required data-validation="required" name="master_label" type="text" placeholder="Master Label">
</div>
<div class="form-group">
    <label class="control-label">Master Value</label>
    <input autocomplete="off" class="form-control" required data-validation="required" name="master_value" type="text" placeholder="Master value">
</div>
<div class="form-group">
    <label class="control-label">Master Category</label>
    <input autocomplete="off" class="form-control" required data-validation="required" name="master_category" type="text" placeholder="Master category">
</div>
