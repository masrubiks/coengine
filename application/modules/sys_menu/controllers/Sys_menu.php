<?php

class Sys_menu extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $menu = $this->m_general->get_menu($this->uri->segment(1));

        $data['title'] = $menu->menu_name;
        $data['icon'] = $menu->menu_icon;

        $breadcrumb = array(
            'name' => $menu->menu_name,
            'url' => $this->uri->segment(1),
        );

        $data['breadcrumb'] = $this->layouts->make_breadcrumb($breadcrumb);
        $data['config'] = $this->m_general->get_row_object('engine_config');
        $data['parent'] = $this->m_general->get_where_order_object('engine_menu', array('menu_parent_id' => null), array('menu_order' => 'asc'));
        $data['child'] = $this->m_general->get_where_order_object('engine_menu', array('menu_parent_id !=' => null), array('menu_order' => 'asc'));
        $data['module'] = $this->m_general->get_object('engine_module');

        $this->layouts->templates('v_index', $data, 'v_head', 'v_foot');
    }

    public function save_add()
    {
        $this->layouts->is_ajax();

        $this->db->trans_begin();

        if ($this->form_validation->run() == false) :
            $result['status'] = false;
            $result['message'] = validation_errors();
            $this->output->set_output(json_encode($result));
        endif;

        $data_insert['menu_name'] = $this->input->post('menu_name');
        $data_insert['menu_order'] = $this->input->post('menu_order');
        $data_insert['menu_icon'] = $this->input->post('menu_icon');

        if ($this->input->post('menu_type') == 1) {
            $data_insert['menu_parent_id'] = NULL;
            if ($this->input->post('menu_module')) :
                $data_insert['menu_module_id'] = $this->input->post('menu_module');
            else :
                $data_insert['menu_module_id'] = NULL;
            endif;
        } else {
            $data_insert['menu_parent_id'] = $this->input->post('menu_parent');
            $data_insert['menu_module_id'] = $this->input->post('menu_module');
        }

        $this->m_general->insert_data('engine_menu', $data_insert);

        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            $result['status'] = true;
            $result['message'] = 'Success';
        } else {
            $this->db->trans_rollback();
            $result['status'] = false;
            $result['message'] = 'Failed';
        }

        $this->output->set_output(json_encode($result));
    }

    public function save_edit()
    {
        $this->layouts->is_ajax();

        $this->db->trans_begin();

        if ($this->form_validation->run() == false) :
            $result['status'] = false;
            $result['message'] = validation_errors();
            $this->output->set_output(json_encode($result));
        endif;

        $data_update['menu_name'] = $this->input->post('menu_name');
        $data_update['menu_order'] = $this->input->post('menu_order');
        $data_update['menu_icon'] = $this->input->post('menu_icon');

        if ($this->input->post('menu_type') == 1) {
            $data_update['menu_parent_id'] = NULL;
            if ($this->input->post('menu_module')) :
                $data_update['menu_module_id'] = $this->input->post('menu_module');
            else :
                $data_update['menu_module_id'] = NULL;
            endif;
        } else {
            $data_update['menu_parent_id'] = $this->input->post('menu_parent');
            $data_update['menu_module_id'] = $this->input->post('menu_module');
        }

        $this->m_general->update_data('engine_menu', $data_update, array('menu_id' => $this->input->post('menu_id')));

        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            $result['status'] = true;
            $result['message'] = 'Success';
        } else {
            $this->db->trans_rollback();
            $result['status'] = false;
            $result['message'] = 'Failed';
        }

        $this->output->set_output(json_encode($result));
    }

    public function delete()
    {
        $this->layouts->is_ajax();

        $this->db->trans_begin();

        $data_delete['menu_id'] = $this->input->post('data_id');

        $this->m_general->delete_data('engine_menu', $data_delete);

        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            $result['status'] = true;
            $result['message'] = 'Success';
        } else {
            $this->db->trans_rollback();
            $result['status'] = false;
            $result['message'] = 'Failed';
        }

        return $this->output->set_output(json_encode($result));
    }

    public function get()
    {
        $this->layouts->is_ajax();

        $result = $this->m_general->get_where_row_array('engine_menu', array('menu_id' => $this->input->post('data_id')));
        if (!empty($result)) {
            $result['status'] = true;
            $result['data'] = $result;
        } else {
            $result['status'] = false;
            $result['data'] = '';
            $result['message'] = 'Failed';
        }
        $this->output->set_output(json_encode($result));
    }
}
