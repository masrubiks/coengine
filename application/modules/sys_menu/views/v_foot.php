<script src="<?= base_url('assets/' . $config->config_template . '/'); ?>plugins/nestable/jquery.nestable.js"></script>
<script src="<?= base_url('assets/' . $config->config_template . '/'); ?>plugins/jquery-ui/jquery-ui.min.js"></script>
<script>
    $('#form-add').submit(function() {
        var modal_id = '#modal-add';
        var modal_form = '#form-add';

        $.ajax({
            url: "<?php echo base_url('sys_menu/save_add'); ?>",
            type: "POST",
            data: $(this).serialize(),
            timeout: 5000,
            dataType: "JSON",
            success: function(data) {
                if (data.status) //if success close modal and reload ajax table
                {
                    toastr.success(data.message);
                    window.setTimeout(function() {
                        location.reload()
                    }, 2000)
                    $(modal_id).modal('hide');
                    $(modal_form)[0].reset();

                } else {
                    toastr.error(data.message);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                toastr.error('Terjadi kesalahan saat menghubungkan ke server.');
            }
        });
    });

    $('#form-edit').submit(function() {
        var modal_id = '#modal-edit';
        var modal_form = '#form-edit';

        $.ajax({
            url: "<?php echo base_url('sys_menu/save_edit'); ?>",
            type: "POST",
            data: $(this).serialize(),
            timeout: 5000,
            dataType: "JSON",
            success: function(data) {
                if (data.status) //if success close modal and reload ajax table
                {
                    toastr.success(data.message);
                    window.setTimeout(function() {
                        location.reload()
                    }, 2000)

                    $(modal_id).modal('hide');
                    $(modal_form)[0].reset();

                } else {
                    toastr.error(data.message);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                toastr.error('Terjadi kesalahan saat menghubungkan ke server.');
            }
        });
    });

    $('.edit-data').on('click', function() {
        var form = '#form-edit';
        var data_id = $(this).attr('data-id');
        $.ajax({
            url: "<?php echo base_url('sys_menu/get') ?>",
            type: "POST",
            data: "data_id=" + data_id,
            timeout: 5000,
            dataType: "JSON",
            success: function(data) {
                if (data.status) {
                    $(form + ' [name="menu_id"]').val(data.menu_id);
                    $(form + ' [name="menu_name"]').val(data.menu_name);
                    $(form + ' [name="menu_order"]').val(data.menu_order);
                    $(form + ' [name="menu_icon"]').val(data.menu_icon);
                    if (data.menu_parent_id) {
                        $('.child').show();
                        $(form + ' [name="menu_type"]').val(0);
                        $(form + ' [name="menu_parent"]').val(data.menu_parent_id);
                        $(form + ' [name="menu_module"]').val(data.menu_module_id);
                    } else {
                        $('.child').hide();
                        $(form + ' [name="menu_type"]').val(1);
                        $(form + ' [name="menu_module"]').val(data.menu_module_id);
                    }
                } else {
                    toastr.error(data.message);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                toastr.error('Terjadi kesalahan saat menghubungkan ke server.');
            }
        });
    });

    $('.delete-data').on('click', function() {
        var data_id = $(this).attr('data-id');
        var data_name = $(this).attr('data-name');

        bootbox.confirm({
            closeButton: false,
            message: "Are you sure to delete " + data_name + "?",
            callback: function(result) {
                if (result) {
                    $.ajax({
                        url: "<?php echo base_url('sys_menu/delete') ?>",
                        type: "POST",
                        data: "data_id=" + data_id,
                        timeout: 5000,
                        dataType: "JSON",
                        success: function(data) {
                            if (data.status) {
                                toastr.success(data.message);
                                window.setTimeout(function() {
                                    location.reload()
                                }, 2000)
                            } else {
                                toastr.error(data.message);
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            dataTableVar.ajax.reload();
                            toastr.error('Terjadi kesalahan saat menghubungkan ke server.');
                        }
                    });
                }
            }
        })
    });

    $(document).ready(function() {
        var updateOutput = function(e) {
            var list = e.length ? e : $(e.target),
                output = list.data('output');
            if (window.JSON) {
                //output.val(window.JSON.stringify(list.nestable('serialize'))); //, null, 2));
            } else {
                output.val('JSON browser support required for this demo.');
            }
        };
        // activate Nestable for list 1
        $('#nestable').nestable({
            group: 1
        }).on('change', updateOutput);

        // activate Nestable for list 2
        $('#nestable2').nestable({
            group: 1
        }).on('change', updateOutput);

        // output initial serialised data
        updateOutput($('#nestable').data('output', $('#nestable-output')));
        updateOutput($('#nestable2').data('output', $('#nestable2-output')));

        $('#nestable-menu').on('click', function(e) {
            var target = $(e.target),
                action = target.data('action');
            if (action === 'expand-all') {
                $('.dd').nestable('expandAll');
            }
            if (action === 'collapse-all') {
                $('.dd').nestable('collapseAll');
            }
        });
    });
</script>