<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><i class="<?= $icon; ?> "></i> <?= $title; ?></h1>
                </div>
                <div class="col-sm-6">
                    <?= $breadcrumb; ?>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">Data <?= $title; ?></h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-body">
                <div class="dd" id="nestable">
                    <ol class="dd-list">
                        <?php foreach ($parent as $parents) : ?>
                            <li class="dd-item" data-id="<?= $parents->menu_id; ?>">
                                <div class="dd-handle">
                                    <?= $parents->menu_name; ?>
                                    <div class="dd-nodrag btn-group ml-auto float-right">
                                        <a href="javascript:void(0)" class="btn btn-xs btn-flat btn-success edit-data" data-toggle="modal" data-target="#modal-edit" data-id="<?= $parents->menu_id; ?>" title="Edit"><i class="fa fa-fw fa-edit"></i></a>
                                        <a href="javascript:void(0)" class="btn btn-xs btn-flat btn-danger delete-data" data-id="<?= $parents->menu_id; ?>" data-name="<?= $parents->menu_name; ?>" title="Edit"><i class="fa fa-fw fa-times"></i></a>
                                    </div>
                                </div>
                                <ol class="dd-list">
                                    <?php foreach ($child as $childs) : ?>
                                        <?php if ($parents->menu_id == $childs->menu_parent_id) : ?>
                                            <li class="dd-item" data-id="<?= $childs->menu_id; ?>">
                                                <div class="dd-handle"><?= $childs->menu_name; ?>
                                                    <div class="dd-nodrag btn-group ml-auto float-right">
                                                        <a href="javascript:void(0)" class="btn btn-xs btn-flat btn-success edit-data" data-toggle="modal" data-target="#modal-edit" data-id="<?= $childs->menu_id; ?>" title="Edit"><i class="fa fa-fw fa-edit"></i></a>
                                                        <a href="javascript:void(0)" class="btn btn-xs btn-flat btn-danger delete-data" data-id="<?= $childs->menu_id; ?>" data-name="<?= $childs->menu_name; ?>" title="Edit"><i class="fa fa-fw fa-times"></i></a>
                                                    </div>
                                                </div>
                                            </li>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </ol>
                            </li>
                        <?php endforeach; ?>
                    </ol>
                </div>

            </div>
            <!-- /.card-body -->
            <div class="card-footer clearfix">
                <div class="btn-group float-right">
                    <button type="button" href="javascript:void(0)" data-toggle="modal" data-target="#modal-add" id="modal" class="btn btn-warning btn-flat"><i class="fas fa-plus"></i> Add</button>
                    <button type="button" class="btn btn-info btn-flat"><i class="fas fa-check"></i> Save</button>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<div class="example-modal">
    <div class="modal fade" id="modal-add" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">ADD MENU</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form class="form" method="post" onsubmit="return false" id="form-add">
                    <div class="modal-body">
                        <?php require 'v_modal.php'; ?>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-flat btn-primary" type="submit">
                            <i class="fa fa-fw fa-check"></i> SAVE
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="example-modal">
    <div class="modal fade" id="modal-edit" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">EDIT MENU</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form class="form" method="post" onsubmit="return false" id="form-edit">
                    <div class="modal-body">
                        <?php require 'v_modal.php'; ?>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-flat btn-primary" type="submit">
                            <i class="fa fa-fw fa-check"></i> SAVE
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>