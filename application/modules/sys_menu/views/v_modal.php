<input class="form-control" name="menu_id" type="hidden">
<div class="form-group">
    <label class="control-label">Menu Name</label>
    <input autocomplete="off" class="form-control" required data-validation="required" name="menu_name" type="text" placeholder="Menu Name">
</div>
<div class="form-group">
    <label class="control-label">Menu Type</label>
    <select class="form-control type select2" name="menu_type" data-validation="required">
        <option value="" disabled selected>Choose Type</option>
        <option value="1">Parent</option>
        <option value="0">Child</option>
    </select>
</div>
<div class="form-group child">
    <label class="control-label">Menu Parent</label>
    <select class="form-control select2" name="menu_parent" data-validation="required">
        <option value="" disabled selected>Choose Menu Parent</option>
        <?php foreach ($parent as $parents) : ?>
            <option value="<?= $parents->menu_id; ?>"><?= $parents->menu_name; ?></option>
        <?php endforeach; ?>
    </select>
</div>
<div class="form-group">
    <label class="control-label">Module</label>
    <select class="form-control select2" name="menu_module" data-validation="required">
        <option value="">No Module</option>
        <?php foreach ($module as $modules) : ?>
            <option value="<?= $modules->module_id; ?>"><?= $modules->module_name; ?></option>
        <?php endforeach; ?>
    </select>
</div>
<div class="form-group">
    <label class="control-label">Menu Order</label>
    <input autocomplete="off" class="form-control" required name="menu_order" type="text" placeholder="Menu Order">
</div>
<div class="form-group">
    <label class="control-label">Menu Icon</label>
    <input autocomplete="off" class="form-control" name="menu_icon" type="text" placeholder="Menu Icon">
</div>