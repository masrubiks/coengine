<?php

class Sys_menuaccess extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_menuaccess');
    }

    public function index()
    {
        $menu = $this->m_general->get_menu($this->uri->segment(1));

        $data['title'] = $menu->menu_name;
        $data['icon'] = $menu->menu_icon;

        $breadcrumb = array(
            'name' => $menu->menu_name,
            'url' => $this->uri->segment(1),
        );

        $data['breadcrumb'] = $this->layouts->make_breadcrumb($breadcrumb);
        $data['url_table'] = base_url() . 'sys_menuaccess/postjson/table';
        $data['menu'] = $this->m_general->get_array('engine_menu');
        $data['role'] = $this->m_general->get_array('engine_role');

        $this->layouts->templates('v_index', $data, 'v_head', 'v_foot');
    }

    public function postjson()
    {
        header('Content-type:application/json');
        $type = $this->uri->segment(3);
        $post = $this->input->post();

        switch ($type) {
            case 'table':
                $this->table($post);
                break;
        }
    }

    public function save_add()
    {
        $this->layouts->is_ajax();

        $this->db->trans_begin();

        if ($this->form_validation->run() == false) :
            $result['status'] = false;
            $result['message'] = validation_errors();
            $this->output->set_output(json_encode($result));
        endif;

        $data_insert['menuaccess_role_id'] = $this->input->post('role_id');
        $data_insert['menuaccess_menu_id'] = $this->input->post('menu_id');

        $check = $this->m_general->get_where_array('engine_menuaccess', 
        array('menuaccess_role_id' => $this->input->post('role_id'), 'menuaccess_menu_id' => $this->input->post('menu_id')));

        if ($check) {
            $result['status'] = false;
            $result['message'] = 'Data Duplicate';
        } else {
            $this->m_general->insert_data('engine_menuaccess', $data_insert);
            
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                $result['status'] = true;
                $result['message'] = 'Success';
            } else {
                $this->db->trans_rollback();
                $result['status'] = false;
                $result['message'] = 'Failed';
            }
        }

        $this->output->set_output(json_encode($result));
    }

    public function save_edit()
    {
        $this->layouts->is_ajax();

        $this->db->trans_begin();

        if ($this->form_validation->run() == false):
            $result['status'] = false;
            $result['message'] = validation_errors();
            $this->output->set_output(json_encode($result));
        endif;

        $data_update['menuaccess_role_id'] = $this->input->post('role_id');
        $data_update['menuaccess_menu_id'] = $this->input->post('menu_id');

        $check = $this->m_general->get_where_array('engine_menuaccess', 
        array('menuaccess_role_id' => $this->input->post('role_id'), 'menuaccess_menu_id' => $this->input->post('menu_id')));

        if ($check) {
            $result['status'] = false;
            $result['message'] = 'Data Duplicate';
        } else {
            $this->m_menuaccess->update_multivalue('engine_menuaccess',
            'menuaccess_role_id', $this->input->post('role_id_old'), 
            'menuaccess_menu_id', $this->input->post('menu_id_old'), 
            $data_update);

            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                $result['status'] = true;
                $result['message'] = 'Success';
            } else {
                $this->db->trans_rollback();
                $result['status'] = false;
                $result['message'] = 'Failed';
            }
        }

        $this->output->set_output(json_encode($result));
    }

    public function delete()
    {
        $this->layouts->is_ajax();

        $this->db->trans_begin();

        $data_delete['menuaccess_role_id'] = $this->input->post('data_role');
        $data_delete['menuaccess_menu_id'] = $this->input->post('data_menu');

        $this->m_general->delete_data('engine_menuaccess', $data_delete);

        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            $result['status'] = true;
            $result['message'] = 'Success';
        } else {
            $this->db->trans_rollback();
            $result['status'] = false;
            $result['message'] = 'Failed';
        }

        return $this->output->set_output(json_encode($result));
    }

    public function get()
    {
        $this->layouts->is_ajax();

        $result = $this->m_general->get_where_row_array('engine_menuaccess', array('menuaccess_role_id' => $this->input->post('data_role'), 'menuaccess_menu_id' => $this->input->post('data_menu')));
        if (!empty($result)) {
            $result['status'] = true;
            $result['data'] = $result;
        } else {
            $result['status'] = false;
            $result['data'] = '';
            $result['message'] = 'Failed';
        }
        $this->output->set_output(json_encode($result));
    }

    private function table()
    {
        $url_update = base_url() . 'sys_menuaccess/update/';
        $url_delete = base_url() . 'sys_menuaccess/delete/';

        $data['select'] = 'role_id, menu_id, role_name, menu_name';
        $data['table'] = 'engine_menuaccess';
        $data['join'] = array(
            'engine_role' => array('engine_menuaccess.menuaccess_role_id = engine_role.role_id', 'left'), 
            'engine_menu' => array('engine_menuaccess.menuaccess_menu_id = engine_menu.menu_id', 'left')
        );
        $data['column_search'] = array('role_name', 'menu_name');
        $data['orders'] = array('role_name' => 'asc');
        $data['column_show'] = array('role_id', 'menu_id','role_name', 'menu_name');

        $datatable = $this->m_general->get_table($data);
        foreach ($datatable['data'] as $value) {
            $action_button = '<div class="btn-group">';
            $action_button .= '<a href="javascript:void(0)" class="btn btn-sm btn-flat btn-success edit-data" data-toggle="modal" data-target="#modal-edit" data-role="' . $value[1] . '" data-menu="' . $value[2] . '" title="Edit"><i class="fa fa-fw fa-edit"></i></a>';
            $action_button .= '<a href="javascript:void(0)" class="btn btn-sm btn-flat btn-danger btn-delete delete-data" data-role="' . $value[1] . '" data-menu="' . $value[2] . '" title="Delete"><i class="fa fa-fw fa-times"></i></a>';
            $action_button .= '</div>';

            $value[] = $action_button;
            $dttbl[] = $value;
        }
        $datatable['data'] = isset($dttbl) ? $dttbl : array();
        $this->output->set_output(json_encode($datatable));
    }
}
