<?php

class M_Menuaccess extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    function update_multivalue($tabel, $kolom, $id, $kolom2, $id2, $data)
    {
        if (empty($tabel) || empty($data) || empty($kolom) || empty($id))
            return false;
        $this->db->where($kolom, $id);
        $this->db->where($kolom2, $id2);
        return $this->db->update($tabel, $data);
    }
}