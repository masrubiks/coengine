<!-- DataTables -->
<script src="<?= base_url('assets/' . $config->config_template . '/'); ?>plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url('assets/' . $config->config_template . '/'); ?>plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
    $(document).ready(function() {
        dataTableVar = $('#table-datatable').DataTable({
            //dom: 'Blfrtip',
            lengthMenu: [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            autoWidth: true,
            processing: true,
            scrollX: true,
            serverSide: true,
            ordering: true,
            order: [3, 'asc'],
            ajax: {
                url: '<?= $url_table; ?>',
                type: 'POST'
            },
            columns: [{
                    width: '1%',
                    orderable: false,
                    className: 'text-center'
                },
                {
                    orderable: true,
                    visible: false
                },
                {
                    orderable: true,
                    visible: false
                },
                {
                    orderable: true
                },
                {
                    orderable: true
                },
                {
                    width: '1%',
                    orderable: false,
                    className: 'text-center'
                }
            ]
        });
    });

    $('#form-add').submit(function() {
        var modal_id = '#modal-add';
        var modal_form = '#form-add';

        $.ajax({
            url: "<?php echo base_url('sys_menuaccess/save_add'); ?>",
            type: "POST",
            data: $(this).serialize(),
            timeout: 5000,
            dataType: "JSON",
            success: function(data) {
                if (data.status) //if success close modal and reload ajax table
                {
                    dataTableVar.ajax.reload();
                    toastr.success(data.message);

                    $(modal_id).modal('hide');
                    $(modal_form)[0].reset();
                } else {
                    toastr.error(data.message);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                toastr.error('Terjadi kesalahan saat menghubungkan ke server.');
            }
        });
    });

    $('#table-datatable').on('click', '.edit-data', function () {
            var form = '#form-edit';
            var data_role = $(this).attr('data-role');
            var data_menu = $(this).attr('data-menu');
            $.ajax({
                url: "<?php echo base_url('sys_menuaccess/get') ?>",
                type: "POST",
                data: "data_role=" + data_role + "&data_menu=" + data_menu,
                timeout: 5000,
                dataType: "JSON",
                success: function (data) {
                    if (data.status) {
                        $(form + ' [name="role_id_old"]').val(data.menuaccess_role_id);
                        $(form + ' [name="menu_id_old"]').val(data.menuaccess_menu_id);
                        $(form + ' [name="role_id"]').val(data.menuaccess_role_id);
                        $(form + ' [name="menu_id"]').val(data.menuaccess_menu_id);
                    } else {
                        toastr.error(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    dataTableVar.ajax.reload();
                    toastr.error('Terjadi kesalahan saat menghubungkan ke server.');
                }
            });
        });

        $('#form-edit').submit(function () {
            var modal_id = '#modal-edit';
            var modal_form = '#form-edit';

            $.ajax({
                url: "<?php echo base_url('sys_menuaccess/save_edit'); ?>",
                type: "POST",
                data: $(this).serialize(),
                timeout: 5000,
                dataType: "JSON",
                success: function (data) {
                    if (data.status) //if success close modal and reload ajax table
                    {
                        dataTableVar.ajax.reload();
                        toastr.success(data.message);

                        $(modal_id).modal('hide');
                        $(modal_form)[0].reset();
                    } else {
                        toastr.error(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error('Terjadi kesalahan saat menghubungkan ke server.');
                }
            });
        });

        $('#table-datatable').on('click', '.delete-data', function() {
            var data_role = $(this).attr('data-role');
            var data_menu = $(this).attr('data-menu');

        bootbox.confirm({
            closeButton: false,
            message: "Are you sure to delete this menu access?",
            callback: function(result) {
                if (result) {
                    $.ajax({
                        url: "<?php echo base_url('sys_menuaccess/delete') ?>",
                        type: "POST",
                        data: "data_role=" + data_role + "&data_menu=" + data_menu,
                        timeout: 5000,
                        dataType: "JSON",
                        success: function(data) {
                            if (data.status) {
                                toastr.success(data.message);
                                dataTableVar.ajax.reload();
                            } else {
                                toastr.error(data.message);
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            dataTableVar.ajax.reload();
                            toastr.error('Terjadi kesalahan saat menghubungkan ke server.');
                        }
                    });
                }
            }
        })
    });
</script>