<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><i class="<?= $icon; ?> "></i> <?= $title; ?></h1>
                </div>
                <div class="col-sm-6">
                    <?= $breadcrumb; ?>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">Data <?= $title; ?></h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                </div>
            </div>
            <div class="card-body">
                <button type="button" href="javascript:void(0)" data-toggle="modal" data-target="#modal-add" id="modal" class="btn btn-sm btn-flat btn-primary float-right ml-2">
                    <i class="fas fa-plus"></i> Add
                </button>
                <table id="table-datatable" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Role Id</th>
                            <th>Menu Id</th>
                            <th>Role Name</th>
                            <th>Menu Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<div class="example-modal">
    <div class="modal fade" id="modal-add" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">ADD MENU ACCESS</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form class="form" method="post" onsubmit="return false" id="form-add">
                    <div class="modal-body">
                        <?php require 'v_modal.php'; ?>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary btn-flat" type="submit">
                            <i class="fa fa-fw fa-check"></i> SAVE
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="example-modal">
    <div class="modal fade" id="modal-edit" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">EDIT MENU ACCESS</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form class="form" method="post" onsubmit="return false" id="form-edit">
                    <div class="modal-body">
                        <?php require 'v_modal.php'; ?>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary btn-flat" type="submit">
                            <i class="fa fa-fw fa-check"></i> SAVE
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>