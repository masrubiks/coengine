<input class="form-control" name="role_id_old" type="hidden">
<input class="form-control" name="menu_id_old" type="hidden">
<div class="form-group">
    <label class="control-label">Role</label>
    <select class="form-control select2" name="role_id" data-validation="required">
        <option value="" disabled selected>Choose Role</option>
        <?php foreach ($role as $roles): ?>
            <option value="<?= $roles['role_id'] ?>"><?= $roles['role_name'] ?></option>
        <?php endforeach; ?>
    </select>
</div>
<div class="form-group">
    <label class="control-label">Menu</label>
    <select class="form-control select2" name="menu_id" data-validation="required">
        <option value="" disabled selected>Choose Menu</option>
        <?php foreach ($menu as $menus): ?>
            <option value="<?= $menus['menu_id'] ?>"><?= $menus['menu_name'] ?></option>
        <?php endforeach; ?>
    </select>
</div>