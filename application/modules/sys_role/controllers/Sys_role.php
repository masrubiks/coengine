<?php

class Sys_role extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $menu = $this->m_general->get_menu($this->uri->segment(1));

        $data['title'] = $menu->menu_name;
        $data['icon'] = $menu->menu_icon;

        $breadcrumb = array(
            'name' => $menu->menu_name,
            'url' => $this->uri->segment(1),
        );

        $data['breadcrumb'] = $this->layouts->make_breadcrumb($breadcrumb);
        $data['url_table'] = base_url() . 'sys_role/postjson/table';
        $data['menu'] = $this->m_general->get_where_array('engine_menu', array('menu_parent_id' => NULL));

        $this->layouts->templates('v_index', $data, 'v_head', 'v_foot');
    }

    public function postjson()
    {
        header('Content-type:application/json');
        $type = $this->uri->segment(3);
        $post = $this->input->post();

        switch ($type) {
            case 'table':
                $this->table($post);
                break;
        }
    }

    public function save_add()
    {
        $this->layouts->is_ajax();

        $this->db->trans_begin();

        if ($this->form_validation->run() == false) :
            $result['status'] = false;
            $result['message'] = validation_errors();
            $this->output->set_output(json_encode($result));
        endif;

        $data_insert['role_name'] = $this->input->post('role_name');
        $data_insert['role_default'] = $this->input->post('role_default');

        $this->m_general->insert_data('engine_role', $data_insert);

        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            $result['status'] = true;
            $result['message'] = 'Success';
        } else {
            $this->db->trans_rollback();
            $result['status'] = false;
            $result['message'] = 'Failed';
        }

        $this->output->set_output(json_encode($result));
    }

    public function save_edit()
    {
        $this->layouts->is_ajax();

        $this->db->trans_begin();

        if ($this->form_validation->run() == false):
            $result['status'] = false;
            $result['message'] = validation_errors();
            $this->output->set_output(json_encode($result));
        endif;

        $data_update['role_name'] = $this->input->post('role_name');
        $data_update['role_default'] = $this->input->post('role_default');

        $this->m_general->update_data('engine_role', $data_update, array('role_id'=> $this->input->post('role_id')));
        
        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            $result['status'] = true;
            $result['message'] = 'Success';
        } else {
            $this->db->trans_rollback();
            $result['status'] = false;
            $result['message'] = 'Failed';
        }

        $this->output->set_output(json_encode($result));
    }

    public function delete()
    {
        $this->layouts->is_ajax();

        $this->db->trans_begin();

        $data_delete['role_id'] = $this->input->post('data_id');

        $this->m_general->delete_data('engine_role', $data_delete);

        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            $result['status'] = true;
            $result['message'] = 'Success';
        } else {
            $this->db->trans_rollback();
            $result['status'] = false;
            $result['message'] = 'Failed';
        }

        return $this->output->set_output(json_encode($result));
    }

    public function get()
    {
        $this->layouts->is_ajax();

        $result = $this->m_general->get_where_row_array('engine_role', array('role_id' => $this->input->post('data_id')));
        if (!empty($result)) {
            $result['status'] = true;
            $result['data'] = $result;
        } else {
            $result['status'] = false;
            $result['data'] = '';
            $result['message'] = 'Failed';
        }
        $this->output->set_output(json_encode($result));
    }

    private function table()
    {
        $url_update = base_url() . 'sys_role/update/';
        $url_delete = base_url() . 'sys_role/delete/';

        $data['select'] = 'role_id, role_name, menu_name';
        $data['table'] = 'engine_role';
        $data['join'] = array('engine_menu' => array('engine_menu.menu_id = engine_role.role_default', 'left'));
        $data['column_search'] = array('role_name', 'menu_name');
        $data['orders'] = array('role_name' => 'asc');
        $data['column_show'] = array('role_id', 'role_name', 'menu_name');

        $datatable = $this->m_general->get_table($data);

        foreach ($datatable['data'] as $value) {
            $button = array(
                array(
                    'button' => 'edit',
                    'id'    => $value[1],
                    'name'    => $value[2],
                    'url'   => $url_update,
                ),
                array(
                    'button' => 'delete',
                    'id'    => $value[1],
                    'name'    => $value[2],
                    'url'   => $url_delete,
                ),
            );

            $action_button = format_actionbutton($button);
            $value[] = $action_button;
            $dttbl[] = $value;
        }
        $datatable['data'] = isset($dttbl) ? $dttbl : array();
        $this->output->set_output(json_encode($datatable));
    }
}
