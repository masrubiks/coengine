<!-- DataTables -->
<script src="<?= base_url('assets/' . $config->config_template . '/'); ?>plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url('assets/' . $config->config_template . '/'); ?>plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
    $(document).ready(function() {
        dataTableVar = $('#table-datatable').DataTable({
            //dom: 'Blfrtip',
            lengthMenu: [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            autoWidth: true,
            processing: true,
            scrollX: true,
            serverSide: true,
            ordering: true,
            order: [2, 'asc'],
            ajax: {
                url: '<?= $url_table; ?>',
                type: 'POST'
            },
            columns: [{
                    width: '1%',
                    orderable: false,
                    className: 'text-center'
                },
                {
                    orderable: true,
                    visible: false
                },
                {
                    orderable: true
                },
                {
                    orderable: true
                },
                {
                    width: '1%',
                    orderable: false,
                    className: 'text-center'
                }
            ]
        });
    });

    $('#form-add').submit(function() {
        var modal_id = '#modal-add';
        var modal_form = '#form-add';

        $.ajax({
            url: "<?php echo base_url('sys_role/save_add'); ?>",
            type: "POST",
            data: $(this).serialize(),
            timeout: 5000,
            dataType: "JSON",
            success: function(data) {
                if (data.status) //if success close modal and reload ajax table
                {
                    dataTableVar.ajax.reload();
                    toastr.success(data.message);

                    $(modal_id).modal('hide');
                    $(modal_form)[0].reset();
                } else {
                    toastr.error(data.message);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                toastr.error('Terjadi kesalahan saat menghubungkan ke server.');
            }
        });
    });

    $('#table-datatable').on('click', '.delete-data', function() {
        var data_id = $(this).attr('data-id');
        var data_name = $(this).attr('data-name');

        bootbox.confirm({
            closeButton: false,
            message: "Are you sure to delete " + data_name + "?",
            callback: function(result) {
                if (result) {
                    $.ajax({
                        url: "<?php echo base_url('sys_role/delete') ?>",
                        type: "POST",
                        data: "data_id=" + data_id,
                        timeout: 5000,
                        dataType: "JSON",
                        success: function(data) {
                            if (data.status) {
                                toastr.success(data.message);
                                dataTableVar.ajax.reload();
                            } else {
                                toastr.error(data.message);
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            dataTableVar.ajax.reload();
                            toastr.error('Terjadi kesalahan saat menghubungkan ke server.');
                        }
                    });
                }
            }
        })
    });

    $('#table-datatable').on('click', '.edit-data', function() {
        var form = '#form-edit';
        var data_id = $(this).attr('data-id');
        $.ajax({
            url: "<?php echo base_url('sys_role/get') ?>",
            type: "POST",
            data: "data_id=" + data_id,
            timeout: 5000,
            dataType: "JSON",
            success: function(data) {
                if (data.status) {
                    $(form + ' [name="role_id"]').val(data.data.role_id);
                    $(form + ' [name="role_name"]').val(data.data.role_name);
                    $(form + ' [name="role_default"]').val(data.data.role_default);
                } else {
                    toastr.error(data.message);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                dataTableVar.ajax.reload();
                toastr.error('Terjadi kesalahan saat menghubungkan ke server.');
            }
        });
    });

    $('#form-edit').submit(function() {
        var modal_id = '#modal-edit';
        var modal_form = '#form-edit';

        $.ajax({
            url: "<?php echo base_url('sys_role/save_edit'); ?>",
            type: "POST",
            data: $(this).serialize(),
            timeout: 5000,
            dataType: "JSON",
            success: function(data) {
                if (data.status) //if success close modal and reload ajax table
                {
                    toastr.success(data.message);

                    $(modal_id).modal('hide');
                    $(modal_form)[0].reset();
                } else {
                    toastr.error(data.message);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                toastr.error('Terjadi kesalahan saat menghubungkan ke server.');
            }
        });
    });
</script>