<input class="form-control" name="role_id" type="hidden">
<div class="form-group">
    <label class="control-label">Role Name</label>
    <input autocomplete="off" class="form-control" data-validation="required" name="role_name" type="text" placeholder="Role Name">
</div>
<div class="form-group">
    <label class="control-label">Menu Default</label>
    <select class="form-control select2" name="role_default" data-validation="required">
        <option value="" disabled selected>Choose Menu</option>
        <?php foreach ($menu as $menus) : ?>
            <option value="<?= $menus['menu_id'] ?>"><?= $menus['menu_name'] ?></option>
        <?php endforeach; ?>
    </select>
</div>