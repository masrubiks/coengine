<footer class="main-footer">
    <div class="float-right d-none d-sm-block">
        <b>Version</b> 3.0.1
    </div>
    <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
    reserved.
</footer>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<script src="<?= base_url('assets/' . $config->config_template . '/'); ?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url('assets/' . $config->config_template . '/'); ?>dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?= base_url('assets/' . $config->config_template . '/'); ?>dist/js/demo.js"></script>
<!-- Toastr -->
<script src="<?= base_url('assets/' . $config->config_template . '/'); ?>plugins/toastr/toastr.min.js"></script>
<script src="<?= base_url('node_modules/bootbox/bootbox.js') ?>"></script>

<?php isset($foot) ? $this->load->view($foot) : ''; ?>

</body>

</html>
<script>
    $(document).ready(function() {
        var urls = window.location.href;
        $.ajax({
            url: "<?php echo base_url('home/create_log'); ?>",
            type: "POST",
            data: "urls=" + urls,
            timeout: 5000,
            dataType: "JSON",
            success: function(data) {},
            error: function(jqXHR, textStatus, errorThrown) {}
        });
    });
</script>