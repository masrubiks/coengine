<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?= base_url('home'); ?>" class="brand-link">
        <img src="<?= base_url('assets/' . $config->config_template . '/'); ?>dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light"><?= $config->config_name; ?></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column nav-flat nav-child-indent" data-widget="treeview" role="menu" data-accordion="false">
                <?php foreach ($list_menu as $key => $value) : ?>
                    <?php if (!empty($value->child)) : ?>
                        <li class="nav-item has-treeview  <?= (!empty($value->expand)) ? 'menu-open' : null; ?>">
                            <a href="#" class="nav-link <?= (!empty($value->expand)) ? 'active' : null; ?>">
                                <i class="nav-icon <?= $value->menu_icon ?>"></i>
                                <p>
                                    <?= $value->menu_name ?>
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <?php foreach ($value->child as $key2 => $value2) : ?>
                                    <li class="nav-item">
                                        <a href="<?= base_url() . $value2->module_name; ?>" class="nav-link <?= (!empty($value2->active)) ? 'active' : null; ?>">
                                            <i class="<?= $value2->menu_icon ?> nav-icon"></i>
                                            <p><?= $value2->menu_name ?></p>
                                        </a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </li>
                    <?php else : ?>
                        <li class="nav-item">
                            <a href="<?= base_url() . $value->module_name; ?>" class="nav-link <?= (!empty($value->active)) ? 'active' : null; ?>">
                                <i class="nav-icon <?= $value->menu_icon; ?>"></i>
                                <p><?= $value->menu_name; ?></p>
                            </a>
                        </li>
                    <?php endif; ?>
                <?php endforeach; ?>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>